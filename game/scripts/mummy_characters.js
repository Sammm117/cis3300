//v1.5.1
Quintus.mummyCharacters = function(Q) {
	Q.Sprite.extend("Rival", {
    init: function(p) {
      this._super(p, {
        type: Q.SPRITE_RACER,
        collisionMask: Q.SPRITE_OTHER
      });
      this.add('2d');
      this.p.vx = 40 + Math.random()*40;  //randomly sets a speed for the mummy.            
    },
  });
  //this sets up the player sprite as a 2D entity using the 2d library of Quintus
  Q.Sprite.extend("Player", {
    init: function(p) {
        this._super(p, { 
          sheet: 'player',
          type: Q.SPRITE_RACER,
          collisionMask: Q.SPRITE_OTHER
        });
        this.add('2d');        
      }
  });
  //this is the sprite which allows for the endGame sequence to take place.
  Q.Sprite.extend("Escape", {
      init: function(p) {
        this._super(p, { 
          type: Q.SPRITE_OTHER,
          collisionMask: Q.SPRITE_OTHER
        });
        this.add('2d');
        //this sets different labels on the end game based on which sprite collides with the doorway first.
        this.on("hit",function(collision) {
          if(collision.obj.isA("Player")) { //if the player hits the object first this happens
            Q.stageScene("endGame",1, { label: "You Won!" }); 
            //this.destroy();
          }
          else if(collision.obj.isA("Rival")) { //if the mummy hits the obect first then this happens
            Q.stageScene("endGame",1, { label: "Game Over!" }); 
            //this.destroy();
          }
        });
      }
  });
};
//v1.5.1
Quintus.mummyUI = function(Q) {
  Q.UI.Text.extend("QuestionText", {
    init: function() {
      this._super({
        color: "white",
        x: 0,
        y: 0,
      });
    },

    generate: function() {

     if (this.p.label == Q.state.get('question1'))
         {
           this.p.label = Q.state.get('question2');
         }
         else if (this.p.label == Q.state.get('question2')){
          this.p.label = Q.state.get('question3');
         }
         else if (this.p.label == Q.state.get('question3')){
          this.p.label = Q.state.get('question4');
         }
         else if (this.p.label == Q.state.get('question4')){
          this.p.label = Q.state.get('question5')
         }
         else {
           this.p.label = Q.state.get('question1');
         }
    },

    getAnswer: function() {
      return "true";
    }
  });

  //answer button
  Q.UI.Button.extend('NumberButton', {
    init: function(p) {
      this._super(Q._defaults(p, {
        fill: "#FFFFFF",
        border: 2,
        shadow: 3,
        shadowColor: "rgba(0,0,0,0.5)",
        w: 40,
        h: 40
      }),                     
      function() {

        var currAnsw = this.p.answerLabel.p.label == "" ? 0 : +"*"; 


          this.p.answerLabel.p.label = "" + (this.p.label);

      }
      );
    }
  });

  Q.scene('ui', function(stage){
    //current question
    var qContainer = stage.insert(new Q.UI.Container({
          fill: "gray",
          x: 155,  
          y: 230,
          border: 2,
          shadow: 3,
          shadowColor: "rgba(0,0,0,0.5)",
          w: 280,
          h: 100
        })
    );

    var question = stage.insert(new Q.QuestionText(),qContainer);
    question.generate();

    //answer area
    var aContainer = stage.insert(new Q.UI.Container({
      fill: "#438700",
      x: 150,
      y: 385,
      border: 2,
      shadow: 3,
      shadowColor: "rgba(0,0,0,0.5)",
      w: 140,
      h: 50
    }));

    var answerLabel = aContainer.insert(new Q.UI.Text({
      label: "",
      color: "white",
      x: 0,
      y: 0
    }));

    //submit answer
    var aButton = stage.insert(new Q.UI.Button({
      fill: "white",
      label: "Enter",
      x: 150,
      y: 445,
      border: 2,
      shadow: 3,
      shadowColor: "rgba(0,0,0,0.5)",
      w: 140,
      h: 50
    }, function() {

      var isCorrect = question.getAnswer() == answerLabel.p.label; 
      var player = Q("Player", 0).first();

      if(isCorrect) {
        Q.audio.play('win.mp3');
        player.p.vx += 10;
      }
      else {
        Q.audio.play('boom.mp3');
        player.p.vx = Math.max(0, player.p.vx - 5);
      }
      answerLabel.p.label = ""; 
      question.generate();
    }
    ));

    //buttons
  
    stage.insert(new Q.NumberButton( //answer labels
    {
      label: 'true',
      w: 140,
      h: 40,
      
      y: 230+80, //height on game area
      x: 221-142,    // depth into game area (far across)
      answerLabel: answerLabel
    }
      ));
      stage.insert(new Q.NumberButton(
    {
      label: 'false',
      w: 140,
      h: 40,
      y: 230+80,
      x: 221,    
      answerLabel: answerLabel
    }
      ));    
  });
}
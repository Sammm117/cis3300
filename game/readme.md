1. to start this game, it will need to be hosted on a server, I used MAMP on my MacOS.
2. Once downloaded MAMP, go to preferences, choose the document root where the game is saved.
3. start the server and go to localhost:8888 or on the mamp webpage that opens choose mywebsite tab and it loads up the game.


Audio licenses:

Boom
created by bareform
https://freesound.org/people/bareform/sounds/218721/
license:
license: CC-BY 3.0 http://creativecommons.org/licenses/by/3.0/

Applause sound
created by joedeshon
https://freesound.org/people/joedeshon/sounds/119032/
license: CC-BY 3.0 http://creativecommons.org/licenses/by/3.0/
